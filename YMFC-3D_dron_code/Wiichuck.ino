#include <Wiichuck.h>
#include <Wire.h>

Wiichuck wii;

int joyX, joyY, accelX, accelY, buttonC, buttonZ;

void setup() {
  Serial.begin(9600);
  wii.init();
  
  wii.calibrate();  // calibration
}

void loop() {

  // Convertimos los datos de salida del nunchuck en porcentajes //
  if (wii.poll()) {
    joyX = wii.joyX() - 125;             
    joyY = (wii.joyY() - 125) * 1.1;
    accelX = (wii.accelX() - 530) / 2;
    accelY = (wii.accelY() - 500) / 2;
    buttonC = wii.buttonC();
    buttonZ = wii.buttonZ();
    
    // Acelerómtero //
    if(joyX > -20 && joyX < 20) {
      joyX = 0;
    }
    if(joyY > -20 && joyY < 20) {
      joyY = 0;
    }
    if(accelX > -20 && accelX < 20) {
      accelX = 0;
    }
    if(accelY > -20 && accelY < 20) {
      accelY = 0;
    }
    if(accelX > 100) {
      accelX = 100;
    }
    if(accelX < -100) {
      accelX = -100;
    }
    if(accelY > 100) {
      accelY = 100;
    }
    if(accelY < -100) {
      accelY = -100;
    }
    
    // Jostick //
    if(joyX > 100) {
      joyX = 100;
    }
    if(joyX < -100) {
      joyX = -100;
    }
    if(joyY > 100) {
      joyY = 100;
    }
    if(joyY < -100) {
      joyY = -100;
    }
    
    
    Serial.print("joy:");
    Serial.print(joyX);
    Serial.print("%, ");
    Serial.print(joyY);
    Serial.print("%  \t");
    
    Serial.print("accle:");
    Serial.print(accelX);
    Serial.print("%, ");
    Serial.print(accelY);
    Serial.print("%, ");
    
    Serial.print("button:");
    Serial.print(buttonC);
    Serial.print(", ");
    Serial.print(buttonZ);
    Serial.println("");
  }
  
  delay(20);
}
