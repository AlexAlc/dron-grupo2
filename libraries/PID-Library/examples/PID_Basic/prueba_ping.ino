
const int pingPin = 7;
const int echoPin = 8;
float duration, cm, cm1, cm2,cm3,cm4,cm5;

void setup() {
  
  Serial.begin(9600);
  
}

void loop() {
  
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
  
  cm5=cm4;
  cm4=cm3;
  cm3=cm2;
  cm2=cm;
  
  cm1 = duration / 29.41176 /2;
  cm=(cm5+cm4+cm3+cm2+cm1)/5;
  
  Serial.println(cm);


}

