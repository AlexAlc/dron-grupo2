#ifndef _MPU_minimal_
#define _MPU_minimal_

#include <Wire.h>
 
//Direccion I2C de la IMU
#define MPU 0x68
 
//Ratios de conversion
#define A_R 16384.0
#define G_R 131.0
 
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779

//Valores filtro complementario
#define WEIGHT_ACCEL 0.2
#define WEIGHT_ANGLE 0.8
 
 class MPU_minimal
 {
     public:
        MPU_minimal(){}
        inline void init()
        {
            Wire.begin();
            Wire.beginTransmission(MPU);
            Wire.write(0x6B);
            Wire.write(0);
            Wire.endTransmission(true);
        }
        inline void getAccel()
        {
            //Leer los valores del Acelerometro de la IMU
            Wire.beginTransmission(MPU);
            Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
            Wire.endTransmission(false);
            Wire.requestFrom(MPU,6,true); //A partir del 0x3B, se piden 6 registros
            AcX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
            AcY=Wire.read()<<8|Wire.read();
            AcZ=Wire.read()<<8|Wire.read();

            //Se calculan los angulos Y, X respectivamente.
            Acc[1] = atan(-1*(AcX/A_R)/sqrt(pow((AcY/A_R),2) + pow((AcZ/A_R),2)))*RAD_TO_DEG;
            Acc[0] = atan((AcY/A_R)/sqrt(pow((AcX/A_R),2) + pow((AcZ/A_R),2)))*RAD_TO_DEG;
        }
        inline void getGyro()
        {
            //Leer los valores del Giroscopio
            Wire.beginTransmission(MPU);
            Wire.write(0x43);
            Wire.endTransmission(false);
            Wire.requestFrom(MPU,4,true); //A diferencia del Acelerometro, solo se piden 4 registros
            GyX=Wire.read()<<8|Wire.read();
            GyY=Wire.read()<<8|Wire.read();

            //Calculo del angulo del Giroscopio
            Gy[0] = GyX/G_R;
            Gy[1] = GyY/G_R;
        }
        inline void applyComplementaryFilter()
        {
            Angle[0] = WEIGHT_ANGLE *(Angle[0]+Gy[0]*dt) + WEIGHT_ACCEL*Acc[0];
            Angle[1] = WEIGHT_ANGLE *(Angle[1]+Gy[1]*dt) + WEIGHT_ACCEL*Acc[1];
        }
        inline void update()
        {
            uint32_t time = micros();
            dt = (double)(time-last_time) / 1000000;
            last_time = time;

            getAccel();
            getGyro();
            applyComplementaryFilter();
        }
        inline float AccelX() {return Acc[0];}
        inline float AccelY() {return Acc[1];}
        inline float GyroX() {return Gy[0];}
        inline float GyroY() {return Gy[1];}
        inline float AngleX() {return Angle[0];}
        inline float AngleY() {return Angle[1];}
        
     private:
     
        //MPU-6050 da los valores en enteros de 16 bits
        //Valores sin refinar
        int16_t AcX, AcY, AcZ, GyX, GyY, GyZ;
        //Valores temporales para delta tiempo
        uint32_t last_time;
        double dt;

        //Angulos
        float Acc[2];
        float Gy[2];
        float Angle[2];
 };
 #endif