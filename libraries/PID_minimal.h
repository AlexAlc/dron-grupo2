#ifndef _PID_minimal_
#define _PID_minimal_

class PID_minimal
{


  public:

  //Constants used in some of the functions below
  #define AUTOMATIC	1
  #define MANUAL	0
  #define DIRECT  0
  #define REVERSE  1

  //commonly used functions **************************************************************************
// * constructor.  links the PID to the Input, Output, and 
//   Setpoint.  Initial tuning parameters are also set here

// * sets PID to either Manual (0) or Auto (non-0)
// * performs the PID calculation.  it should be
//   called every time loop() cycles. ON/OFF and
//   calculation frequency can be set using SetMode
//   SetSampleTime respectively
//clamps the output to a specific range. 0-255 by default, but
//it's likely the user will want to change this depending on
//the application

    inline PID_minimal(double* Input, double* Output, double* Setpoint,double Kp, double Ki, double Kd, int ControllerDirection)
    {  
        myOutput = Output;
        myInput = Input;
        mySetpoint = Setpoint;
        inAuto = false;
        
        SetOutputLimits(0, 255);				//default output limit corresponds to 
                                                    //the arduino pwm limits

        SampleTime = 100;							//default Controller Sample Time is 0.1 seconds

        SetControllerDirection(ControllerDirection);
        SetTunings(Kp, Ki, Kd);

        lastTime = millis()-SampleTime;				
    }
	
    inline void SetMode(int Mode)
    {
        bool newAuto = (Mode == AUTOMATIC);
        if(newAuto == !inAuto)
        {  /*we just went from manual to auto*/
            Initialize();
        }
        inAuto = newAuto;
    }

    inline bool Compute()
    {
        if(!inAuto) return false;
        unsigned long now = millis();
        unsigned long timeChange = (now - lastTime);
        if(timeChange>=SampleTime)
        {
            /*Compute all the working error variables*/
            double input = *myInput;
            double error = *mySetpoint - input;
            ITerm+= (ki * error);
            if(ITerm > outMax) ITerm= outMax;
            else if(ITerm < outMin) ITerm= outMin;
            double dInput = (input - lastInput);
        
            /*Compute PID Output*/
            double output = kp * error + ITerm- kd * dInput;
            
            if(output > outMax) output = outMax;
            else if(output < outMin) output = outMin;
            *myOutput = output;
            
            /*Remember some variables for next time*/
            lastInput = input;
            lastTime = now;
            return true;
        }
        else return false;
    }
                       
    inline void SetOutputLimits(double Min, double Max)
    {
        if(Min >= Max) return;
        outMin = Min;
        outMax = Max;
        
        if(inAuto)
        {
            if(*myOutput > outMax) *myOutput = outMax;
            else if(*myOutput < outMin) *myOutput = outMin;
            
            if(ITerm > outMax) ITerm= outMax;
            else if(ITerm < outMin) ITerm= outMin;
        }
    }
					
  //available but not commonly used functions ********************************************************
// * While most users will set the tunings once in the 
//   constructor, this function gives the user the option
//   of changing tunings during runtime for Adaptive control
// * Sets the Direction, or "Action" of the controller. DIRECT
//   means the output will increase when error is positive. REVERSE
//   means the opposite.  it's very unlikely that this will be needed
//   once it is set in the constructor.
// * sets the frequency, in Milliseconds, with which 
//   the PID calculation is performed.  default is 100

    inline void SetTunings(double Kp, double Ki, double Kd)
    {
        if (Kp<0 || Ki<0 || Kd<0) return;
        
        dispKp = Kp; dispKi = Ki; dispKd = Kd;
        
        double SampleTimeInSec = ((double)SampleTime)/1000;  
        kp = Kp;
        ki = Ki * SampleTimeInSec;
        kd = Kd / SampleTimeInSec;
        
        if(controllerDirection ==REVERSE)
        {
            kp = (0 - kp);
            ki = (0 - ki);
            kd = (0 - kd);
        }
    }
                                          
    inline void SetControllerDirection(int Direction)
    {
        if(inAuto && Direction !=controllerDirection)
        {
            kp = (0 - kp);
            ki = (0 - ki);
            kd = (0 - kd);
        }   
        controllerDirection = Direction;
    }			

    inline void SetSampleTime(int NewSampleTime)
    {
        if (NewSampleTime > 0)
        {
            double ratio  = (double)NewSampleTime/ (double)SampleTime;
            ki *= ratio;
            kd /= ratio;
            SampleTime = (unsigned long)NewSampleTime;
        }
    }
                						  
  //Display functions ****************************************************************
	inline double GetKp(){ return  dispKp; }                    // These functions query the pid for interal values.
	inline double GetKi(){ return  dispKi;}                     //  they were created mainly for the pid front-end,
	inline double GetKd(){ return  dispKd;}                     // where it's important to know what is actually 
	inline int GetMode(){ return  inAuto ? AUTOMATIC : MANUAL;} //  inside the PID.
	inline int GetDirection(){ return controllerDirection;}     //

  private:
    inline void Initialize()
    {
        ITerm = *myOutput;
        lastInput = *myInput;
        if(ITerm > outMax) ITerm = outMax;
        else if(ITerm < outMin) ITerm = outMin;
    }
	
	double dispKp;				// * we'll hold on to the tuning parameters in user-entered 
	double dispKi;				//   format for display purposes
	double dispKd;				//
    
	double kp;                  // * (P)roportional Tuning Parameter
    double ki;                  // * (I)ntegral Tuning Parameter
    double kd;                  // * (D)erivative Tuning Parameter

	int controllerDirection;

    double *myInput;              // * Pointers to the Input, Output, and Setpoint variables
    double *myOutput;             //   This creates a hard link between the variables and the 
    double *mySetpoint;           //   PID, freeing the user from having to constantly tell us
                                  //   what these values are.  with pointers we'll just know.
			  
	unsigned long lastTime;
	double ITerm, lastInput;

	unsigned long SampleTime;
	double outMin, outMax;
	bool inAuto;
};
#endif

