unsigned long loop_timer;
unsigned long timer_channel_1, timer_channel_2, esc_timer, esc_loop_timer;
int esc_1, esc_2;

void setup 
{
    DDRD |= B00110000;      //Configure digital port 4 and 5 as output
    
    //We don't want the esc's to be beeping annoyingly.
    PORTD |= B00110000;                        //Set digital port 4 and 5 high.
    delayMicroseconds(1000);                   //Wait 1000us.
    PORTD &= B00001111;                        //Set digital port 4 and 5 low.
    delay(3);                                  //Wait 3 milliseconds before the next loop.
}

void loop 
{
    //All the information for controlling the motor's is available.
  //The refresh rate is 250Hz. That means the esc's need there pulse every 4ms.
  while(micros() - loop_timer < 4000);                              //We wait until 4000us are passed.
  loop_timer = micros();                                            //Set the timer for the next loop.

  PORTD |= B00110000;                                               //Set digital outputs 4 and 5 high.
  timer_channel_1 = esc_1 + loop_timer;                             //Calculate the time of the faling edge of the esc-1 pulse.
  timer_channel_2 = esc_2 + loop_timer;                             //Calculate the time of the faling edge of the esc-2 pulse.
  
  while(PORTD >= 16){                                               //Stay in this loop until output 4 and 5 are low.
    esc_loop_timer = micros();                                      //Read the current time.
    if(timer_channel_1 <= esc_loop_timer)PORTD &= B11101111;        //Set digital output 4 to low if the time is expired.
    if(timer_channel_2 <= esc_loop_timer)PORTD &= B11011111;        //Set digital output 5 to low if the time is expired.
  }
}